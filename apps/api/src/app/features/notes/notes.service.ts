import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Note } from './note.schema';
import { CreateNoteDTO } from './note.dto';

@Injectable()
export class NotesService {
  constructor(
    @InjectModel(Note.name) private readonly model: Model<Note>,
  ) {}

  async create(entity: CreateNoteDTO): Promise<Note> {
    const createdEntity = new this.model(entity);
    return createdEntity.save();
  }

  async findAll(): Promise<Note[]> {
    return this.model.find().exec();
  }

  async findById(id: string): Promise<Note | null> {
    return this.model.findById(id).exec();
  }
}
