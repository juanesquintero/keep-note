export const notes = [
  {
    "id": "134343qrw-42213tu123hk-234kj",
    "title": 'Nota 1',
    "description": '...',
    "createdAt": "2023-04-16",
    "updatedAt": "2023-05-01",
  },
  {
    "id": "12343jl-423hkj-234h24-234kj",
    "title": "Nota 2",
    "description": "...",
    "createdAt": "2023-04-15",
    "updatedAt": "2023-04-25",
  }
]
