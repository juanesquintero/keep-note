import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { NotesService } from './notes.service';
import { Note } from './note.schema';
import { RedisCacheService } from '../../core/services/redis-cache.service';
import { CreateNoteDTO } from './note.dto';

@ApiTags('notes')
@Controller('notes')
export class NotesController {
  constructor(
    private readonly redisService: RedisCacheService,
    private readonly notesService: NotesService,
  ) { }


  @ApiOperation({ summary: 'Get all notes' })
  @ApiResponse({ status: 200, description: 'Returns an array of notes', type: [Note] })
  @Get('/')
  async getAll(): Promise<Note[]> {
    return this.redisService.fetch(
      'notes',
      this.notesService.findAll.bind(this.notesService)
    )
  }

  @ApiOperation({ summary: 'Create a new note' })
  @ApiResponse({ status: 201, description: 'The created note', type: Note })
  @Post('/')
  async createOne(@Body() createNoteDto: CreateNoteDTO): Promise<Note> {
    return this.notesService.create(createNoteDto);
  }
}
