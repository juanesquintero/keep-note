import { Module } from '@nestjs/common';
import { NotesService } from './notes/notes.service';
import { NotesController } from './notes/notes.controller';
import { Note, NoteSchema } from './notes/note.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisCacheService } from '../core/services/redis-cache.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: Note.name, schema: NoteSchema }])],
  exports: [NotesService],
  controllers: [NotesController],
  providers: [NotesService, RedisCacheService],
})
export class FeaturesModule { }
