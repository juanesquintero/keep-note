import { Injectable } from '@nestjs/common';
import { RedisService } from '@liaoliaots/nestjs-redis';

@Injectable()
export class RedisCacheService {
  constructor(
    private readonly redisService: RedisService
  ) {}

  async get(key: string): Promise<any> {
    const client = this.redisService.getClient();
    const value = await client.get(key);
    return value ? JSON.parse(value) : null;
  }

  async set(key: string, value: any): Promise<boolean> {
    const client = this.redisService.getClient();
    const result = await client.set(key, JSON.stringify(value), 'EX', 60);
    return result === 'OK';
  }

  async fetch(key: string, dbFetch: any) {
    const cachedValue = await this.get(key);
    if (cachedValue) {
      return cachedValue;
    }
    const dbValue = await dbFetch();
    await this.set(key, dbValue);
    return dbValue;
  }
}
