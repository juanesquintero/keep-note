import * as dotenv from 'dotenv';
import mongoConfig from './mongo.config';
import redisConfig from './redis.config';

dotenv.config();

const appConfig  = () => ({
  port: parseInt(process.env.API_PORT || '3000', 10),
  prefix: 'api',
});

export {
  appConfig,
  mongoConfig,
  redisConfig
}

