import * as dotenv from 'dotenv';
import { ConfigModule, ConfigService } from '@nestjs/config'
import { MongooseModuleOptions } from '@nestjs/mongoose';

dotenv.config();

const { MONGO_DBNAME, MONGO_HOST, MONGO_PORT, MONGO_USER, MONGO_PSSWD } = process.env;

const db = {
  host: MONGO_HOST || 'localhost',
  port: MONGO_PORT || '27017',
  name: MONGO_DBNAME || 'keep_note',
  user: MONGO_USER,
  pwd: MONGO_PSSWD,
};

const mongoConfig: MongooseModuleOptions = {
  uri: `mongodb://${db.user}:${db.pwd}@${db.host}:${db.port}/${db.name}?authSource=admin`,
  config: {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
};

export default mongoConfig;
