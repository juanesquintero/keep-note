import * as dotenv from 'dotenv';
import { RedisModuleOptions } from 'nestjs-redis';

dotenv.config();

const { REDIS_DB, REDIS_HOST, REDIS_PORT, REDIS_PSSWD } = process.env;

const redisConfig: { config: RedisModuleOptions } = {
  config:
  {
    host: REDIS_HOST || 'localhost',
    port: parseInt(REDIS_PORT || '6379'),
    db: parseInt(REDIS_DB || '0'),
    password: REDIS_PSSWD,
  }
}
export default redisConfig;
