import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import {appConfig, mongoConfig, redisConfig} from './core/config';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { FeaturesModule } from './features/features.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
    }),
    MongooseModule.forRoot(mongoConfig.uri, mongoConfig.config),
    RedisModule.forRoot(redisConfig),
    FeaturesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
